import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { CollapseModule } from 'ngx-bootstrap';
import { UsersComponent } from './users/users.component';
import { TableModule } from 'primeng/table';
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    CollapseModule,
    TableModule 
  ],
  declarations: [NavbarComponent, FooterComponent, UsersComponent],
  exports: [
    CommonModule,
    NavbarComponent,
    FooterComponent
  ]
})
export class ComponentsModule { }
