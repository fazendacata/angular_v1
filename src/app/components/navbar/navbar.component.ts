import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public user: any;
  constructor() { 
    this.user = {
      id: 8,
      name: 'Rodolfo Bueno' 
    }
  }

  ngOnInit() {
  }

}
