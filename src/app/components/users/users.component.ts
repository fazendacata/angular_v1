import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  cols = [
    {field: 'name', header: 'Nome'}, 
    {field: 'email', header: 'E-mail'},
    {field: 'phone', header: 'Phone'},
  ]
  users = [
    {name: 'Arthur Busqueiro', email: 'arthurbusqueiro@gmail.com', phone: '65467687'},
    {name: 'Rodolfo Bueno', email: 'rbocvg@gmail.com', phone: '65465413'}
  ]
  constructor() { }

  ngOnInit() {
  }

}
